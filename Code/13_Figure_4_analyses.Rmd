---
title: "GP120-rabbit analysis-> Figure 4"
author: 
  - "Richard Barfield"
  - "Biostatistics, Epidemiology and Research Design (BERD) Core"
  - "Center for Human Systems Immunology (CHSI)"
date: "`r  Sys.Date()`"
output: 
  html_document:
    toc: true
    toc_float: true
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r Baseinfo,eval=FALSE}
#General info ----------------------
#
# Created: 2022-02-03
#
# Author: Richard Barfield
#
# Program: Code/13_Figure_4_analyses.R
# 
# Description: Analyses for Figure 4
 
#--- --- --- --- --- --- --- --- --- --- --- --- --- 
#--- --- --- --- --- --- --- --- --- --- --- --- --- 
```

```{r libs,message=FALSE, warning=FALSE, include=F, results='asis'}

library(lubridate)
library(DescTools)
library(coin)

library(geepack)
library(gee)
library(geesmv)
library(DT)
library(ComplexHeatmap)
library(RColorBrewer)
library(nparLD)
library(table1)
library(readxl)
library(tidyxl)
library(knitr)
library(kableExtra)
library(tidyverse)

```


Analyses for Figure 4


# Path to Data

```{r loaddata}


pathtofile<-"Data/Processed/"
pathtofile<-file.path(getwd(),"../",pathtofile)


#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Load the data
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

Fig4.data<-readRDS(file.path(pathtofile,
                             "0_data_processed_fig4.RDS"))



```

# Figure 4C

```{r fig4c, options}

Analysis<-Fig4.data$Figure4_glycan %>% 
  mutate(Rabbit=str_split_fixed(ID,"_",2)[,1]) %>% 
  filter(str_detect(name,"IgG",negate=F)) 


Analysis %>% ggplot(aes(x=Vaccine,y=MeanValue))+
  geom_boxplot(outlier.shape = NA)+
  geom_point(position=position_jitter(0.1,seed=20210941))+
  facet_wrap(~name,nrow=3,ncol=3,scales="free_y")+
  theme(axis.text.x = element_text(size=6,angle=90))

```


## Running the analysis

```{r runanalyss, options}
OtherBit<-Fig4.data$Figure4_total_wk0 %>% 
  filter(str_detect(name,"IgG",negate=F)) %>% 
  mutate(Rabbit=str_split_fixed(ID,"_",2)[,1]) 


NewAnalysis<-Analysis %>% 
  left_join(OtherBit %>% select(Rabbit,name,wk0val=MeanValue)) %>% 
  mutate(MeanValue=MeanValue/wk0val)%>% 
  group_by(name) %>% 
  mutate(Vaccine=as.factor(Vaccine)) %>% 
  summarize(Pvalue=pvalue(wilcox_test(MeanValue ~ Vaccine,dist="exact"))[1]) %>% 
  mutate(FDR.adjust=p.adjust(Pvalue,"BH")) %>% 
  arrange(Pvalue)

NewAnalysis %>% select(name,AdjWk0Pvalue=Pvalue,FDR.adjust) %>% 
  DT::datatable(rownames=F) %>% 
  DT::formatSignif(c("AdjWk0Pvalue","FDR.adjust"))


```



# Session Info

```{r sessioninfo}
sessionInfo()
```
