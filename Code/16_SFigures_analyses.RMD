---
title: "GP120-rabbit analysis-> Supplementary Figures"
author: 
  - "Richard Barfield"
  - "Biostatistics, Epidemiology and Research Design (BERD) Core"
  - "Center for Human Systems Immunology (CHSI)"
date: "`r  Sys.Date()`"
output: 
  html_document:
    toc: true
    toc_float: true
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

```{r Baseinfo,eval=FALSE}
#General info ----------------------
#
# Created: 2022-02-03
#
# Author: Richard Barfield
#
# Program: Code/16_SFigures_analyses.R
#
# Description: Analyses for Supplement figures
#                
#--- --- --- --- --- --- --- --- --- --- --- --- --- 
#--- --- --- --- --- --- --- --- --- --- --- --- --- 
```

```{r libs,message=FALSE, warning=FALSE, include=F, results='asis'}


library(coin)

library(geepack)
library(gee)
library(geesmv)

library(ggfortify)
library(rcompanion)
library(matrixcalc)
library(nparLD)
library(stats)
library(DT)
library(knitr)
library(kableExtra)
library(tidyverse)

```

# Introduction

Reading in and processing the data into an analytical form to be used

# Path to Data

```{r loaddata}



pathtofile<-"Data/Processed/"
pathtofile<-file.path(getwd(),"../",pathtofile)




```

# Figure S13B

First visualization (only 16, there is a duplcate metabolite in plot in SAP).

```{r Aim42analysis, echo=F,fig.height=12,fig.width=12}


#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Load the data
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

FigS.data<-readRDS(file.path(pathtofile,
                             "0_data_processed_sup_Figures.RDS"))





```





## Run the analysis

```{r analysisFigS6A, options}

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Run the p-value
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  



Opvalue<-FigS.data$FigS6A %>% 
   group_by(TheBinding,Week) %>% 
   summarize(pvalue_coin_exact=pvalue(wilcox_test(Value~as.factor(Vaccine),
            distribution="exact"))[1],
            .groups="drop") %>% 
  mutate(FDR_Pvalue=p.adjust(pvalue_coin_exact,"BH"))

Opvalue %>% 
  mutate(Week=factor(Week,levels=c("wk2","wk8","wk14"))) %>% 
  arrange(Week) %>% 
  DT::datatable(rownames=F) %>% 
  DT::formatSignif(c("pvalue_coin_exact","FDR_Pvalue"),3)



```

# Figure S13C


```{r FigS6B, options}



Other.data<-FigS.data$FigS6B_FcR 


OurPvalues<-Other.data %>% 
  group_by(What) %>% 
   summarize(pvalue=pvalue(wilcox_test(Mean~as.factor(Vaccine),
            distribution="exact"))[1],
            .groups="drop") %>% 
  mutate(FDR_Pvalue=p.adjust(pvalue,"BH"))

OurPvalues%>% 
  DT::datatable(rownames=F) %>% 
  DT::formatSignif(c("pvalue","FDR_Pvalue"),3)
```


# Figure S15

See Figure 6 analysis. 


# Figure S16A

```{r FigS8A, echo=F}

# Fig3.data<-readRDS(file.path(pathtofile,
#                              "0_data_processed_fig4.RDS"))


Aim32<-FigS.data$FigS8A.actual_save.spr

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Setting data to lower bound
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
ForAnalysis<-Aim32 %>% filter(name %in% c("kd","Avidity")) %>% 
  mutate(Week=as.character(Week)) %>% 
  rename("subject"="Sample")

Resu.KD<-nparLD(Newvalue~Week+Vaccine,time1.order = c("2","8","14"),
                          subject = "subject",order.warning = F,
                          description = F,
                          data=ForAnalysis %>% filter(name=="kd"))
            
 
Resu.Avid<-nparLD(Newvalue~Week+Vaccine,time1.order = c("2","8","14"),
                          subject = "subject",order.warning = F,
                          description = F,
                          data=ForAnalysis %>% filter(name!="kd"))
            
 

            

data.frame(Pvalues=c(signif(Resu.KD$ANOVA.test.mod.Box[4],3),
                     signif(Resu.Avid$ANOVA.test.mod.Box[4],3))) %>% 
  mutate(What=c("kd","Avidiity")) %>% 
  select("What","Pvalues") %>% 
  kable() %>% kable_styling(full_width = F)



ForAnalysis %>% 
  mutate(AtBound=ifelse(AtUpperBound==1|AtLowerBound==1,"At Bound","Not At Bound")) %>% 
  mutate(Week=factor(Week,levels=c("2","8","14"))) %>% 
  ggplot(aes(x=Week,y=Newvalue+1,col=Vaccine))+
  geom_boxplot(outlier.shape = NA)+
  geom_point(position = position_jitterdodge(0.1,seed=20210895),size=2)+
  facet_wrap(~name,nrow=2,scales = "free_y")+
  scale_y_log10()

```


# Figure S16B

```{r FigureS8B, }
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# source code 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
source("1_general_code_for_calc_BC_variance.R")



HoldThis<-readRDS(file.path(pathtofile,
                             "0_data_processed_fig2.RDS"))
Fig3.Bama <- HoldThis$Figure2_BAMA

Fig3.Bama.Analyze<-Fig3.Bama %>% select(Rabbit,Week,Vaccine,
                     c1086V1V2=`c1086 V1/V2 tag (20)`,
                     V3C=`Bio-V3.C (45)`,
                     C1=`C1-biotin (51)`,
                     V21086=`Bio-V2.1086.c (42)`,
                     C5.2C=`Bio. Rv144.C5.2C (43)`) %>% 
  pivot_longer(cols=c(-Rabbit,-Week,-Vaccine)) 



Fig3.Bama.Analyze %>% 
  filter(!is.na(value)) %>% 
  ggplot(aes(x=name,y=value,col=Vaccine))+
  geom_boxplot(outlier.shape = NA)+
  geom_point(position=position_jitterdodge(0.1))+
  facet_wrap(~Week,ncol=3)+
  scale_y_log10()+
  theme(legend.position = "bottom",axis.text.x = element_text(angle = 90))+
  xlab("")+
  ylab("MFI")
  

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Do this analyses using code from the BAMA analysis with gp120
# From Programs/GitCode/26_BAMA_multivalency_20200831
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---


ForGee<-Fig3.Bama.Analyze %>% ungroup() %>%
  filter(!is.na(value)) %>% 
  mutate(BB=log10(value)) %>%
  mutate(NewMouse=factor(Rabbit)) %>% 
  mutate(Week=as.character(Week)) %>% 
  mutate(Vaccine=ifelse(str_detect(Vaccine,"gp120-Q11"),1,0)) %>% 
  mutate(Week=factor(Week,levels=c("2","8","14")))




my.formula<-BB~-1+name+Week*Vaccine


NewGEE<-ForGee %>% mutate(Rabbit=str_sub(Rabbit,2,-1))
NewGEE$subject<-NewGEE$Rabbit
NewGEE$id<-NewGEE$Rabbit
NewGEE<-NewGEE[order(NewGEE$subject),]

NewResus<-MyGee.Var.MD(my.formula,id="id",family=stats::gaussian,
                       data=NewGEE)

Contrast.est2<-matrix(0,nrow=3,ncol=length(NewResus$beta_est))

Contrast.est2[1:3,which(str_detect(names(NewResus$beta_est),"Vaccine") &
                          str_detect(names(NewResus$beta_est),"Week",negate=T))]<-1

Contrast.est2[2,which(str_detect(names(NewResus$beta_est),"Vaccine") &
                          str_detect(names(NewResus$beta_est),"Week8"))]<-1
Contrast.est2[3,which(str_detect(names(NewResus$beta_est),"Vaccine") &
                          str_detect(names(NewResus$beta_est),"Week14"))]<-1


Our.Var<-Contrast.est2 %*% NewResus$cov.beta  %*% t(Contrast.est2)

Our.New.effects<-Contrast.est2 %*%NewResus$beta_est



Overall.test<-t(Our.New.effects) %*% solve(Our.Var) %*% Our.New.effects
Overall.Pvalue<-pchisq(Overall.test[1,1],df=3,lower.tail=F)

Ind.Pvalues<-pchisq(Our.New.effects^2/diag(Our.Var),df=1,lower.tail=F)


data.frame(What=c("Week 2 Vaccine: Q11 vs no","Week 8 Vaccine: Q11 vs no",
                  "Week 14 Vaccine: Q11 vs no"),stringsAsFactors = F) %>% 
  mutate(Effect_Estimate=Our.New.effects,
         Effect_Std=sqrt(diag(Our.Var)),
         Effect_Pvalue=Ind.Pvalues,
         Effect_Adjust_Pvalue=p.adjust(Ind.Pvalues,"bonferroni")) %>% 
  DT::datatable(rownames = F) %>% 
  DT::formatSignif(c("Effect_Estimate","Effect_Std","Effect_Pvalue","Effect_Adjust_Pvalue"))

```

# Session Info

```{r sessioninfo}
sessionInfo()
```
